if ! command -v git >/dev/null; then
  printf "Please install git first! I. e.:\n";
  printf "  sudo apt install git\n";
  exit 1
fi
