git clone https://gitlab.com/alerc/oh-my-compose.git ~/.oh-my-compose
touch ~/.XCompose
echo "include \"/home/$USER/.oh-my-compose/arrows\"" >> ~/.XCompose
echo "include \"/home/$USER/.oh-my-compose/common\"" >> ~/.XCompose
echo "include \"/home/$USER/.oh-my-compose/diacritics\"" >> ~/.XCompose
echo "include \"/home/$USER/.oh-my-compose/math\"" >> ~/.XCompose
echo "include \"/home/$USER/.oh-my-compose/music\"" >> ~/.XCompose
echo "include \"/home/$USER/.oh-my-compose/offtop\"" >> ~/.XCompose
