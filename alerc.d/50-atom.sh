if command -v "apm" >/dev/null; then
  git clone "https://gitlab.com/alerc/atom.git" "~/.atom"
  apm install --packages-file "~/.atom/package.list"
else
  printf "Atom not found, skipping\n";
fi
